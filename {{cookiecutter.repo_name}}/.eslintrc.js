module.exports = {
    env: {
        {%-if cookiecutter.target == 'es6' %}
        es6: true,
        {%-else %}
        es6: false,
        {%-endif %}
        {%-if cookiecutter.env == 'browser+node' %}
        browser: true,
        node: true,
        {%-elif cookiecutter.env == 'browser' %}
        browser: true,
        {%-else %}
        node: true,
        {%-endif %}
    },
    {%-if cookiecutter.linter == 'eslint+prettier' %}
    extends: ["airbnb-base", "eslint:recommended", "prettier"],
    plugins: ["prettier"],
    {%-else %}
    extends: ["airbnb-base", "eslint:recommended"],
    {%-endif %}
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: "module",
    },
    {%-if cookiecutter.test_framework == 'jest' %}
    overrides: [
        {
            files: ["**/*.spec.js"],
            env: {
                jest: true,
            },
        },
    ],
    {%-endif %}
    rules: {
        {%-if cookiecutter.linter == 'eslint+prettier' %}
        "prettier/prettier": [
            "error",
            {
                endOfLine: "auto",
                tabWidth: 4,
                arrowParens: "avoid",
                trailingComma: "all",
            },
        ],
        {%-endif %}
        quotes: ["error", "double"],
        indent: [
            "error",
            4,
            {
                SwitchCase: 1,
            },
        ],
        "no-console": "off",
        "linebreak-style": "off",
        "no-unused-expressions": [
            "error",
            {
                allowShortCircuit: true,
            },
        ],
        "arrow-parens": ["error", "as-needed"],
        "func-names": "off",
        "no-plusplus": [
            "error",
            {
                allowForLoopAfterthoughts: true,
            },
        ],
        "no-param-reassign": [
            "error",
            {
                props: false,
            },
        ],
        "no-underscore-dangle": [
            "error",
            {
                allowAfterThis: true,
            },
        ],
    },
};
