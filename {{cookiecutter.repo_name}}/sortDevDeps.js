function sortObject(obj) {
    const out = Object.create(null);

    Object.keys(obj)
        .sort(function (a, b) {
            return a.toLowerCase().localeCompare(b.toLowerCase());
        })
        .forEach(function (key) {
            out[key] = obj[key];
        });

    return out;
}
(function () {
    const save = require("fs").writeFileSync;
    const pkgPath = process.cwd() + "/package.json";
    const pkg = require(pkgPath);

    if (pkg.devDependencies) {
        console.log(" > Sorting devDependencies...");
        pkg.devDependencies = sortObject(pkg.devDependencies);
        console.log(JSON.stringify(pkg.devDependencies, null, "  "));
    }

    console.log(" > Writing package.json...");
    save(pkgPath, JSON.stringify(pkg, null, "    "));

    console.log(" > Done");
})();
