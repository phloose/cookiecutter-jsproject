const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    entry: "./src/index.js",
    output: {
        filename: "index.js",
        path: path.resolve(__dirname, "dist"),
        publicPath: "/dist/",
    },
    devtool: "source-map",
    {%- if cookiecutter.use_webpack_dev_server == 'yes' %}
    devServer: {
        index: "./index.html",
        publicPath: "./src",
        writeToDisk: true,
    },
    {%- endif %}
    plugins: [
        new MiniCssExtractPlugin({
            filename: "{{ cookiecutter.package_name }}.css",
        }),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, "css-loader"],
            },
            {
                test: /\.(jpg|png|gif|svg)$/,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 8000,
                            name: "images/[hash]-[name].[ext]",
                        },
                    },
                ],
            },
        ],
    },


};
