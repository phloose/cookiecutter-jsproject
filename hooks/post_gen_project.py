
import os
import shutil
import subprocess
import sys


def unlink_if_exists(path):
    if os.path.exists(path):
        os.unlink(path)


def rmtree_if_exists(path):
    if os.path.exists(path):
        shutil.rmtree(path)


if __name__ == "__main__":

# vscode

{%- if cookiecutter.use_vscode != "yes" %}
    rmtree_if_exists(".vscode")
{%- endif %}

# Test framework

{%- if cookiecutter.test_framework != "jest" %}
    unlink_if_exists("jest.config.js")
    unlink_if_exists("babel.config.js")
    rmtree_if_exists("__tests__")
{%- else %}
{%- if cookiecutter.use_es6_modules != "yes" %}
    unlink_if_exists("babel.config.js")
{%- endif %}
{%- endif %}

# Linter

{%- if cookiecutter.linter not in ["eslint", "eslint+prettier"] %}
    unlink_if_exists(".eslintrc.js")
{%- endif %}

# Bundler

{%- if cookiecutter.env == 'node' or cookiecutter.bundler != "webpack" %}
    unlink_if_exists("webpack.config.js")
{%- endif %}

    subprocess.run(["node", "sortDevDeps.js"], shell=True, check=True)
    unlink_if_exists("sortDevDeps.js")

    print("""
################################################################################
################################################################################

    You have succesfully created `{{ cookiecutter.repo_name }}`.

################################################################################

    You've used these cookiecutter parameters:
{% for key, value in cookiecutter.items()|sort %}
        {{ "{0:26}".format(key + ":") }} {{ "{0!r}".format(value).strip("u") }}
{%- endfor %}

    See .cookiecutterrc for instructions on regenerating the project.

################################################################################

    To get started run these:

        cd {{ cookiecutter.repo_name }}
        git init
        git add .
        git commit -m "Initial commit"
        git remote add origin git@{{ cookiecutter.repo_hosting_domain }}:{{ cookiecutter.repo_username }}/{{ cookiecutter.repo_name }}.git
        git checkout -b dev
        git push -u origin master dev

""")
